#!/usr/bin/env python3

# Copyright 2020 Tomas Åkesson <tomas@entropic.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from bs4 import BeautifulSoup
import markdown
import mimetypes
import os
import subprocess
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

m4_include(src/resources.py)
m4_include(src/ui_mainwindow.py)
m4_include(src/ui_about.py)

mimetypes.init()


class About(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)
        self.ui.but_about_close.clicked.connect(self.accept)


class MarkdownBrowser(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent=parent)
        self.setupUi(self)

        if len(sys.argv) > 1:
            self.home_url = os.path.abspath(sys.argv[1])
        else:
            self.home_url = ""

        self.basedir = os.path.dirname(self.home_url)
        self.current_url = self.home_url

        self.history = []
        self.browser.anchorClicked['QUrl'].connect(self.on_link_clicked)
        self.browser.highlighted['QUrl'].connect(self.on_link_highlighted)
        self.button_back.pressed.connect(self.on_back_clicked)
        self.button_home.pressed.connect(self.on_home_clicked)
        self.button_edit.pressed.connect(self.on_edit_clicked)

        self.go_home()
        self.update_ui()

    # Enables/disables some elements depending on the state of other elements
    def update_ui(self):
        self.button_back.setEnabled(
            len(self.history) > 1
        )
        self.button_home.setEnabled(
            self.current_url != self.home_url
        )
        self.button_edit.setEnabled(
            os.path.isfile(self.current_url)
        )

    ### Event handlers

    def on_link_clicked(self, url):
        self.go_to(url.toString())

    def on_link_highlighted(self, url):
        self.statusbar.showMessage(url.toString())

    def on_home_clicked(self):
        self.go_home()

    def on_back_clicked(self):
        self.go_back()

    def on_edit_clicked(self):
        self.edit_file()

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction();

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            file_list = []
            for url in event.mimeData().urls():
                filename = url.toLocalFile()
                if filename != "":
                    self.go_to(filename)
                    return # only load the first file

    def keyPressEvent(self, event):
        if event.modifiers() == QtCore.Qt.ControlModifier:
            if event.key() == QtCore.Qt.Key_Q:
                self.close()
        elif event.key() == QtCore.Qt.Key_Escape:
            self.close()
        elif event.key() == QtCore.Qt.Key_Home:
            self.go_home()
        elif event.key() == QtCore.Qt.Key_Backspace:
            self.go_back()
        elif event.key() == QtCore.Qt.Key_F5:
            self.go_to(self.current_url)
        elif event.key() == QtCore.Qt.Key_F1:
            about = About(self)
            about.exec()

    ### Functions

    def edit_file(self):
        message = edit_file(self.current_url)
        self.statusbar.showMessage(message)

    def go_back(self):
        try:
            self.history.pop()
            url = self.history.pop()
            self.go_to(url)
        except:
            pass

    def go_home(self):
        self.go_to(self.home_url)

    def go_to(self, url):
        if url == "":
            return

        self.basedir = new_basedir(self.basedir, url)
        full_url = self.basedir + "/" + os.path.basename(url)
        full_url = add_index(full_url)
        self.load_url(full_url)

    def load_url(self, url):
        # To get images (with relative paths) to show, we must cd to the
        # document dir
        if os.path.isdir(self.basedir):
            os.chdir(self.basedir)
       
        html = get_html(url)
        self.browser.setHtml(html)
        self.current_url = url
        self.history = add_to_history(url, self.history)
        self.update_ui()
        self.setWindowTitle(get_document_title(html) + " - Markdown Browser")

# end of class


def edit_file(filename):
    test_editors = [
        "/usr/bin/retext",
        "/usr/bin/kwrite",
    ]

    editor = None
    for test in test_editors:
        if os.path.isfile(test):
            editor = test
            break
    if editor is not None:
        process = subprocess.Popen([editor, filename])
        return QApplication.translate(
            "MarkdownBrowser",
            "Starting editor. Press F5 to reload when file is saved."
        )
    else:
        editors_text = ', '.join(test_editors)
        return QApplication.translate(
            "MarkdownBrowser",
            "Error: Could not find an editor. Tried {0}"
        ).format(editors_text)

def add_to_history(url, history):
    if history is None:
        history = []
    try:
        last = history[-1]
    except:
        last = ""
    if last != url:
        history.append(url)
    return history

# Get document title from the first <h1> tag
def get_document_title(html):
    soup = BeautifulSoup(html, "html.parser")
    try:
        return soup.find("h1").text
    except:
        return ""

def get_html(url):
    dummy, ext = os.path.splitext(url)
    if ext == ".html":
        html = read_file(url)
    else:
        if ext == ".md":
            text = read_file(url)
        elif is_image(url):
            text = f"![Image]({url})"
        elif is_text(url):
            text = "<pre>" + read_file(url) + "</pre>"
        else:
            text = "# " + QApplication.translate(
                "MarkdownBrowser", "File type not supported"
            )
        html = md_to_html(text)
    return html

# If url is a directory, try to find a default file in the directory
def add_index(url):
    if os.path.isdir(url):
        test_index_files = [
            url + "README.md",
            url + "index.md",
        ]

        for file in test_index_files:
            if os.path.isfile(file):
                url = file
                break
    return url

def new_basedir(old_basedir, url):
    new_basedir = old_basedir
    if url.startswith("/"):
        new_basedir = os.path.dirname(url)
    elif "/" in url:
        new_basedir = old_basedir + "/" + os.path.dirname(url)
    else:
        pass
    return new_basedir

def md_to_html(text):
    return markdown.markdown(text, extensions=[
        "tables",
        "fenced_code"
    ])

def read_file(url):
    try:
        with open(url, "r", encoding="utf-8") as input_file:
            return input_file.read()
    except:
        return "# " + QApplication.translate(
            "MarkdownBrowser", "Could not load file"
        )

def is_image(file_name):
    return is_mimetype(file_name, 'image')

def is_text(file_name):
    return is_mimetype(file_name, 'text')

def is_mimetype(file_name, type):
    mimetype = mimetypes.guess_type(file_name)[0]
    if mimetype != None:
        mimestart = mimetype.split('/')[0]
        if mimestart == type:
            return True
    return False


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    # app.setWindowIcon(QtGui.QIcon(":/images/logo.png"))

    locale = QLocale.system().name()
    tr = QTranslator()
    if tr.load("markdownbrowser_" + locale, ":/i18n/"):
        QCoreApplication.installTranslator(tr)


    w = MarkdownBrowser()
    w.show()
    sys.exit(app.exec_())
