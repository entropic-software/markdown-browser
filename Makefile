APP_NAME = markdownbrowser

VERSION:=$(shell cat VERSION)
ifeq ($(CI_COMMIT_SHORT_SHA),)
	CI_COMMIT_SHORT_SHA:=$(shell git rev-parse --short HEAD)
endif
COMPILE_DATE:=$(shell LC_TIME=C date +"%F %T %z")
SHORT_DATE:=$(shell LC_TIME=C date +"%Y%m%d")

DEB_VERSION = $(VERSION)-$(SHORT_DATE)
DEB_PATH = debian/$(APP_NAME)
DEB_PKG = debian/$(APP_NAME).deb
DEB_PKG_FINAL = debian/$(APP_NAME)_$(DEB_VERSION)_all.deb

markdownbrowser.py: src/markdownbrowser.in.py \
src/ui_mainwindow.py \
src/ui_about.py \
src/resources.py
	m4  -D M4_APP_VERSION="$(VERSION)" \
		-D M4_GIT_REVISION="$(CI_COMMIT_SHORT_SHA)" \
		-D M4_COMPILE_DATE="$(COMPILE_DATE)" \
		-P src/markdownbrowser.in.py >markdownbrowser.py
	chmod +x markdownbrowser.py

src/ui_about.py: src/ui_about.ui
	pyuic5 -o src/ui_about.py src/ui_about.ui

src/resources.py: resources.qrc i18n/markdownbrowser_sv_SE.qm
	pyrcc5 -o src/resources.py resources.qrc

i18n/markdownbrowser_sv_SE.qm: i18n/markdownbrowser_sv_SE.ts
	lrelease i18n/markdownbrowser_sv_SE.ts -qm i18n/markdownbrowser_sv_SE.qm


# Run this when strings has been added/updated in source. Will update
# i18n/markdownbrowser_sv_SE.ts with new and updated strings, these should
# be updated or completed, then run "make"
trupdate: src/markdownbrowser.in.py src/ui_mainwindow.py
	pylupdate5 src/markdownbrowser.in.py src/ui_mainwindow.py -ts i18n/markdownbrowser_sv_SE.ts


install: markdownbrowser.py
	mkdir -p /usr/local/bin/
	mkdir -p /usr/local/share/applications
	install -m 755 markdownbrowser.py /usr/local/bin/markdownbrowser
	install ./markdownbrowser.desktop /usr/local/share/applications

uninstall:
	rm -f /usr/local/bin/markdownbrowser.py
	rm -f /usr/local/share/applications/markdownbrowser.desktop


deb: markdownbrowser.py deb-clean
	mkdir -p $(DEB_PATH)/usr/bin
	cp markdownbrowser.py $(DEB_PATH)/usr/bin/markdownbrowser
	chmod +x $(DEB_PATH)/usr/bin/markdownbrowser

	mkdir -p $(DEB_PATH)/usr/share/applications
	cp se.entropic.markdownbrowser.desktop $(DEB_PATH)/usr/share/applications/

	mkdir -p $(DEB_PATH)/usr/share/doc/$(APP_NAME)
	cp LICENSE $(DEB_PATH)/usr/share/doc/$(APP_NAME)/copyright
	cp README.md $(DEB_PATH)/usr/share/doc/$(APP_NAME)/

	mkdir -p $(DEB_PATH)/DEBIAN
	chmod 755 $(DEB_PATH)/DEBIAN
	sed <debian/control.tpl "s/{VERSION}/$(DEB_VERSION)/" >$(DEB_PATH)/DEBIAN/control
	dpkg-deb --root-owner-group --build $(DEB_PATH)
	mv $(DEB_PKG) $(DEB_PKG_FINAL)
	echo $(DEB_PKG_FINAL) >debian/package_file_name

lintian:
	lintian -X changelog-file,manpages $(DEB_PKG_FINAL)


test: markdownbrowser.py test.py
	./test.py


clean:
	rm -f markdownbrowser.py
	rm -f i18n/markdownbrowser_sv_SE.qm
	rm -f src/resources.py
	rm -f src/ui_about.py
	rm -rf __pycache__

deb-clean:
	rm -rf $(DEB_PATH)/usr
	rm -rf debian/*.deb
