# Markdown Browser

A Qt-based browser for markdown files. Will load a markdown-file and display
it like a web-page. Links to other markdown-files can be followed like a
web-browser.


## Screenshot

![Screenshot](screenshot.png)


## Requirements

- Python 3.6
- PyQt5
- BeautifulSoup4

Install requirements on Debian with the command:

`apt install python3-pyqt5 python3-bs4 python3-markdown`

### Requirements for building

`apt install pyqt5-dev-tools qttools5-dev-tools qt5-default m4 make`

## Author

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/markdown-browser
