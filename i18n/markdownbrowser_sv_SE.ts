<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="sv" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/ui_mainwindow.py" line="40"/>
        <source>Back</source>
        <translation>Tillbaks</translation>
    </message>
    <message>
        <location filename="../src/ui_mainwindow.py" line="41"/>
        <source>Home</source>
        <translation>Hem</translation>
    </message>
    <message>
        <location filename="../src/ui_mainwindow.py" line="42"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
</context>
<context>
    <name>MarkdownBrowser</name>
    <message>
        <location filename="../src/markdownbrowser.in.py" line="175"/>
        <source>Starting editor. Press F5 to reload when file is saved.</source>
        <translation>Startar redigerare. Tryck F5 för att ladda om sidan när filen är sparad.</translation>
    </message>
    <message>
        <location filename="../src/markdownbrowser.in.py" line="181"/>
        <source>Error: Could not find an editor. Tried {0}</source>
        <translation>Fel: Kunde inte starta redigerare. Försökte med {0}</translation>
    </message>
    <message>
        <location filename="../src/markdownbrowser.in.py" line="219"/>
        <source>File type not supported</source>
        <translation>Filtypen stöds ej</translation>
    </message>
    <message>
        <location filename="../src/markdownbrowser.in.py" line="260"/>
        <source>Could not load file</source>
        <translation>Kunde inte ladda filen</translation>
    </message>
</context>
</TS>
