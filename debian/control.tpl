Package: markdownbrowser
Version: {VERSION}
Section: text
Priority: optional
Architecture: all
Depends: python3, python3-pyqt5, python3-bs4, python3-markdown
Maintainer: Tomas Åkesson <tomas@entropic.se>
Homepage: https://gitlab.com/entropic-software/markdown-browser
Description: Qt-based browser for markdown files
 Will load a markdown-file and display it like a web-page.
 Links to other markdown-files can be followed like a web-browser.
