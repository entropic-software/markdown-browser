#!/usr/bin/env python3

import unittest
from markdownbrowser import *

class TestFunctions(unittest.TestCase):

    # get_title

    def test_get_title_1(self):
        html = "<h1>hello</h1>"
        expected = "hello"
        got = get_document_title(html)
        self.assertEqual(expected, got)

    def test_get_title_2(self):
        html = "<h1>hello1</h1><h1>hello2</h1>"
        expected = "hello1"
        got = get_document_title(html)
        self.assertEqual(expected, got)

    def test_get_title_3(self):
        html = "<p>para</p><h1>hello</h1>"
        expected = "hello"
        got = get_document_title(html)
        self.assertEqual(expected, got)

    def test_get_title_4(self):
        html = "<h2>hello</h2>"
        expected = ""
        got = get_document_title(html)
        self.assertEqual(expected, got)

    def test_get_title_5(self):
        html = ""
        expected = ""
        got = get_document_title(html)
        self.assertEqual(expected, got)

    def test_get_title_6(self):
        html = "<h1></h1><h1>hello</h1>"
        expected = ""
        got = get_document_title(html)
        self.assertEqual(expected, got)

    # add_to_history

    def test_add_to_history_1(self):
        history = []
        add = "1"
        expected = ["1"]
        got = add_to_history(add, history)
        self.assertEqual(expected, got)

    def test_add_to_history_2(self):
        history = ["1"]
        add = "1"
        expected = ["1"]
        got = add_to_history(add, history)
        self.assertEqual(expected, got)

    def test_add_to_history_3(self):
        history = ["1", "2", "3"]
        add = "4"
        expected = ["1", "2", "3", "4"]
        got = add_to_history(add, history)
        self.assertEqual(expected, got)

    def test_add_to_history_4(self):
        history = ["1", "2", "3"]
        add = "3"
        expected = ["1", "2", "3"]
        got = add_to_history(add, history)
        self.assertEqual(expected, got)

    def test_add_to_history_5(self):
        history = None
        add = "1"
        expected = ["1"]
        got = add_to_history(add, history)
        self.assertEqual(expected, got)

    # new_basedir

    def test_new_basedir_1(self):
        basedir = "/home/user"
        url = "file.md"
        expected = "/home/user"
        got = new_basedir(basedir, url)
        self.assertEqual(expected, got)

    def test_new_basedir_2(self):
        basedir = "/home/user"
        url = "/tmp/subdir/file.md"
        expected = "/tmp/subdir"
        got = new_basedir(basedir, url)
        self.assertEqual(expected, got)

    def test_new_basedir_3(self):
        basedir = "/home/user"
        url = "subdir/file.md"
        expected = "/home/user/subdir"
        got = new_basedir(basedir, url)
        self.assertEqual(expected, got)

    def test_new_basedir_4(self):
        basedir = "/home/user"
        url = ""
        expected = "/home/user"
        got = new_basedir(basedir, url)
        self.assertEqual(expected, got)

    def test_new_basedir_5(self):
        basedir = ""
        url = ""
        expected = ""
        got = new_basedir(basedir, url)
        self.assertEqual(expected, got)

    def test_new_basedir_6(self):
        basedir = "http://www.example.com/path"
        url = "subdir/README.md"
        expected = "http://www.example.com/path/subdir"
        got = new_basedir(basedir, url)
        self.assertEqual(expected, got)

    # md_to_html

    def test_md_to_html_1(self):
        markdown = "# hello"
        expected = "<h1>hello</h1>"
        got = md_to_html(markdown)
        self.assertEqual(expected, got)

    # mimetypes

    def test_is_image(self):
        expected = True
        for filename in ["a.png", "a.jpg", "a.gif"]:
            got = is_image(filename)
            self.assertEqual(expected, got)

    def test_is_image_path(self):
        expected = True
        for filename in ["/a/b.png", "/a/b.jpg", "/a/b.gif"]:
            got = is_image(filename)
            self.assertEqual(expected, got)

    def test_is_not_image(self):
        expected = False
        for filename in ["a.html", "a.mp4", "/a/b.html", "/a/b.mp4"]:
            got = is_image(filename)
            self.assertEqual(expected, got)

    def test_is_not_text(self):
        expected = False
        for filename in ["a.png", "a.mp4", "/a/b.png", "/a/b.mp4"]:
            got = is_text(filename)
            self.assertEqual(expected, got)

    def test_is_text(self):
        expected = True
        for filename in ["a.html", "a.md", "a.txt"]:
            got = is_text(filename)
            self.assertEqual(expected, got)

    def test_is_text_path(self):
        expected = True
        for filename in ["/a/b.html", "/a/b.md", "/a/b.txt"]:
            got = is_text(filename)
            self.assertEqual(expected, got)



if __name__ == '__main__':
    unittest.main()
